# coding:utf-8
from django import forms

# from models import RegistrationProfile
from django.contrib.auth.forms import AuthenticationForm
from django.forms.utils import ErrorList

from models import User, RegistrationProfile

attrs_dict = {'class': 'required'}
USERNAME_USED_MESSAGE = u'Этот логин уже используется. Пожалуйста, введите другой.'
EMAIL_USED_MESSAGE = u'"Этот email уже используется. Пожалуйста введите другой.'


class RegAuthenticationForm(AuthenticationForm):
    u""" class for authenticating users. Extend this to get a form that accepts
        username/password logins.
    """
    username = forms.CharField(
        label=u"Имя пользователя",
        max_length=254
    )
    password = forms.CharField(
        label=u"Пароль",
        widget=forms.PasswordInput
    )

    error_messages = {
        'invalid_login': "Пожалуйста, введите правильное имя пользователя и пароль. "
                         "Обратите внимание, что оба поля могут быть чувствительны к регистру.",
        'inactive': "Этот аккаунт не активен.",
    }


class RegistrationForm(forms.Form):
    u"""
        Форма для регистрации новой учетной записи пользователя.         

        Проверяет, что запрашиваемая имя пользователя не используется,
        и требуется пароль, который необходимо ввести в два раза.         
    """
    username = forms.RegexField(
        regex=r'^\w+$',
        max_length=30,
        widget=forms.TextInput(attrs=attrs_dict),
        label=u'Логин'
    )
    email = forms.EmailField(
        widget=forms.TextInput(attrs=dict(attrs_dict, maxlength=75)),
        label=u'Email'
    )
    password1 = forms.CharField(
        widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
        label=u'Пароль'
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
        label=u'Пароль (повтор)'
    )

    def __init__(self, request, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=None,
                 empty_permitted=False, field_order=None):
        super(RegistrationForm, self).__init__(data, files, auto_id, prefix,
                 initial, error_class, label_suffix,
                 empty_permitted, field_order)

    def clean_username(self):
        u""" Проверяет что имя пользователя состоит из букв и не используется
        """
        try:
            User.objects.get(username__iexact=self.cleaned_data['username'])
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(USERNAME_USED_MESSAGE)

    def clean_password2(self):
        u""" Проверяет, что значения, введены в оба поля и введенные в обоих полях пароля значения идентичны
        """
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(u'Вы должны ввести одинаковый пароль в двух полях.')
        return self.cleaned_data['password2']

    def clean_email(self):
        u""" Проверяет что email уникальный
        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(EMAIL_USED_MESSAGE)

        return self.cleaned_data['email']

    def save(self, profile_callback=None):
        u""" Создает нового пользователя и возвращает объект User
        """
        new_user = RegistrationProfile.objects.create_inactive_user(
            username=self.cleaned_data['username'],
            password=self.cleaned_data['password1'],
            email=self.cleaned_data['email'],
            profile_callback=profile_callback
        )
        return new_user


# class RegistrationFormUniqueEmail(RegistrationForm):
#     """
#     Subclass of ``RegistrationForm`` which enforces uniqueness of
#     email addresses.
#
#     """
#     def clean_email(self):
#         """
#         Validate that the supplied email address is unique for the
#         site.
#
#         """
#         if User.objects.filter(email__iexact=self.cleaned_data['email']):
#             # u'This email address is already in use.
#             # Please supply a different email address.'
#             raise forms.ValidationError(_(u'"Этот email уже используется. '
#                                           u'Пожалуйста введите другой.'))
#         return self.cleaned_data['email']


# class RegistrationFormTermsOfService(RegistrationForm):
#     """
#     Subclass of ``RegistrationForm`` which adds a required checkbox
#     for agreeing to a site's Terms of Service.
#     """
#     tos = forms.BooleanField(
#         widget=forms.CheckboxInput(attrs=attrs_dict),
#         label=_(u'I have read and agree to the Terms of Service'),
#         error_messages={'required': u"You must agree to the terms to reguser"})


# class RegistrationFormUniqueEmail(RegistrationForm):
#     """
#     Subclass of ``RegistrationForm`` which enforces uniqueness of
#     email addresses.
#
#     """
#     def clean_email(self):
#         """
#         Validate that the supplied email address is unique for the
#         site.
#
#         """
#         if User.objects.filter(email__iexact=self.cleaned_data['email']):
#             raise forms.ValidationError(_(u'This email address is already in use. Please supply a different email address.'))
#         return self.cleaned_data['email']


# class RegistrationFormNoFreeEmail(RegistrationForm):
#     """
#     Subclass of ``RegistrationForm`` which disallows registration with
#     email addresses from popular free webmail services; moderately
#     useful for preventing automated spam registrations.
#
#     To change the list of banned domains, subclass this form and
#     override the attribute ``bad_domains``.
#
#     """
#     bad_domains = ['aim.com', 'aol.com', 'email.com', 'gmail.com',
#                    'googlemail.com', 'hotmail.com', 'hushmail.com',
#                    'msn.com', 'mail.ru', 'mailinator.com', 'live.com']
#
#     def clean_email(self):
#         """
#         Check the supplied email address against a list of known free
#         webmail domains.
#
#         """
#         email_domain = self.cleaned_data['email'].split('@')[1]
#         if email_domain in self.bad_domains:
#             raise forms.ValidationError(_(u'Registration using free email addresses is prohibited. Please supply a different email address.'))
#         return self.cleaned_data['email']
